import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator

mpl.use("Agg")


def get_column_data(filepath, col_number):
    col_data = []
    with open(filepath, "r") as f:
        next(f)  # Skip column header line
        for line in f:
            columns = line.split()
            col_data.append(columns[col_number])

    return col_data


def plot(data):
    plt.figure(figsize=(60, 10))
    plt.xscale("linear")
    plt.autoscale(True, "x", True)

    plt.xlabel("Simulation time (ns)")
    plt.ylabel("Value")
    plt.xticks(rotation=45, ha="right")
    plt.yticks([0, 1])

    plt.vlines(data, ymin=0, ymax=1, linewidth=1)

    ax = plt.gca()  # get current axis
    ax.xaxis.set_major_locator(MaxNLocator(200))
    ax.ticklabel_format(useOffset=False, style="plain")

    # plt.show()
    plt.savefig("test.png", bbox_inches="tight")
    plt.close()


def main():
    timestamps = get_column_data("./logs/timefile.log", 2)
    timestamps = [int(x) for x in timestamps]

    plot(timestamps)


if __name__ == "__main__":
    main()
