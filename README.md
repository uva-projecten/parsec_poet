# parsec poet
Dockerfile for compiling and running the POET (and heartbeats) supplemented PARSEC suite.

# Usage
For now, only the blackscholes benchmark is compiled.
```
docker run -it --rm parsec_poet:0.1 -c gcc-hooks-poet -a run -p parsec.blackscholes
```

# TODO
Don't pre-compile any benchmarks and set-up for proper volume support, so the docker container can be used as an actual executable.

