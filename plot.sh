#!/bin/bash
# This script assumes that the docker-compose command results in heartbeat logs produced in the logs/heartbeat.log file.

docker-compose up

cat logs/*.log | awk 'NR>1{print $3}' > logs/hb_timestamps.txt
cat logs/hb_timestamps.txt | sed 's/^..........//g' > logs/hb_small.txt

gnuplot -c plot.gp

