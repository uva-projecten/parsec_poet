# Build heartbeat and POET enabled PARSEC programs.
# Currently just builds blackscholes program.
#
# Note: Final image is large, because it includes the full source of the PARSEC source.

# Build and dependencies and PARSEC program(s)
FROM debian:stable-20230202-slim AS build

RUN apt-get update -y && apt-get install build-essential git cmake m4 -y

WORKDIR /build
RUN git clone https://github.com/libheartbeats/heartbeats.git
RUN git clone https://github.com/libpoet/poet.git
RUN git clone https://github.com/jurrewolff/parsec-3.0.git

WORKDIR /build/heartbeats
RUN make
RUN make install

WORKDIR /build/poet
RUN git checkout baseline-1.0
RUN make install

WORKDIR /build/parsec-3.0
RUN git checkout blackscholes-testing
RUN ./bin/parsecmgmt -c gcc-hooks-poet -p parsec.blackscholes -a build

# Create final environment
FROM debian:stable-20230202-slim
WORKDIR /app

COPY --from=build /usr/local/lib/libhb-* /usr/local/lib/
COPY --from=build /usr/local/include/heartbeats/* /usr/local/include/heartbeats/

COPY --from=build /usr/local/lib/libpoet.so /usr/local/lib/
COPY --from=build /usr/local/include/poet/* /usr/local/include/poet/

COPY --from=build /build/parsec-3.0/ ./

RUN ldconfig # Reload shared libraries (benchmark programs failed to find them for some reason)

RUN mkdir /var/log/heartbeat
ENV HEARTBEAT_ENABLED_DIR="/var/log/heartbeat"

ENTRYPOINT ["./bin/parsecmgmt"]

