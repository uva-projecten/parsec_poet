set xdata time
set timefmt "%s"

set autoscale x

set xlabel "Time"
set ylabel "Value"
plot "./logs/hb_small.txt" using 1:(1) with impulses

