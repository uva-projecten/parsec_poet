import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np

mpl.use("Agg")


def get_column_data(filepath, col_number, header_row):
    col_data = []
    with open(filepath, "r") as f:
        if header_row:
            next(f)  # Skip column header row

        for line in f:
            columns = line.split()
            col_data.append(columns[col_number])

    return col_data


def get_interval_diffs(data):
    if not isinstance(data, list):
        raise TypeError("data must be a list of integers")

    diffs = []
    prev_element = None

    for cur_element in data:
        if not isinstance(cur_element, int):
            raise TypeError("found cur_element of list that is not of integer type")

        if prev_element is None:
            prev_element = cur_element
            continue

        diffs.append(cur_element - prev_element)
        prev_element = cur_element

    return diffs


def plot(data):
    # Freedman-Diaconis rule
    q1, q3 = np.percentile(data, [25, 75])
    iqr = q3 - q1
    n = len(data)
    bin_size = 2 * iqr / (n ** (1 / 3))

    # Plot it!
    plt.figure(figsize=(60, 10))
    plt.hist(data, bins=int(bin_size), color="blue", edgecolor="black")
    plt.title("Histogram")
    plt.xlabel("Interval differences (ns)")
    plt.ylabel("Frequency")
    plt.xticks(rotation=45, ha="right")

    ax = plt.gca()
    ax.xaxis.set_major_locator(MaxNLocator(bin_size))
    ax.ticklabel_format(useOffset=False, style="plain")

    plt.savefig("./results/histogram.png", bbox_inches="tight")
    plt.close()

    # plt.show()


def main():
    file = "/home/jurrew/Documents/Backup/thesis/hotsniper-results/Static-Collection/results_2023-05-18_20.12_4GHz+nothermal+hb_enabled_parsec-blackscholes-simsmall-1/0.hb.log"

    timestamps = get_column_data(file, 2, True)
    timestamps = [int(x) for x in timestamps]

    interval_diffs = get_interval_diffs(timestamps)

    plot(interval_diffs)


if __name__ == "__main__":
    main()
